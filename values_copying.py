#!/usr/bin/env python
"""
  @file     values_copying.py
  @brief    Executable file for run of the script
  @author   Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version  $0.01 $
  @date     $Date: 21.05.2018 $
"""
import sys, glob, getopt, os

sys.path.insert( 0, 'model' )

from DBManager import DBManager
from Descriptor import Descriptor

# reading arguments
opts, args = getopt.getopt( sys.argv[ 1: ], 'i:c:o:q:', [ 'inputfile=', 'changingfile=', 'outputfile=', 'query=' ] )
for opt in opts:
    if opt[0] == '--inputfile' or opt[0] == '-i':
        infile = opt[1]
    elif opt[0] == '--changingfile' or opt[0] == '-c':
        chfile = opt[1]
    elif opt[0] == '--outputfile' or opt[0] == '-o':
        outfile = opt[1]
    elif opt[0] == '--query' or opt[0] == '-q':
        query = opt[1]
        
        
        
query_data = query.split(':')
tbname = query_data[0]
values = query_data[1].split(',')

     


db = DBManager()


indata = db.preinput(infile, 1)    # ID of the input detectors.dat will be 1
db.datinput(indata)
chdata = db.preinput(chfile, 2)   # ID of the modifying detectors.dat will be 2
db.datinput(chdata)


cur = db.connection.cursor()

cur.execute("SELECT unique_key, tbname, "+query_data[1]+" FROM `datfile_detectors` WHERE `file_id`=1 AND `tbname_proj` LIKE '"+tbname+"%'")
rows = cur.fetchall()

for row in rows:
    up_values = ''
    for i, value in enumerate(values):
        if i != 0:
            up_values = up_values + ','
        up_values = up_values + value + '=' + str(row[i+2])
        print('Replacing "' + value + '" for "'+ row[1] +'". New value is ' + str(row[i+2]) )
    #print("UPDATE `datfile_detectors` SET " + up_values + " WHERE `file_id`=2 AND `unique_key`='"+row[0]+"'")
    cur.execute("UPDATE `datfile_detectors` SET " + up_values + " WHERE `file_id`=2 AND `unique_key`='"+row[0]+"'")


db.connection.commit()
db.datoutput(2, outfile) # 2 since it is the ID of the modifying detectors.dat

db.close()
