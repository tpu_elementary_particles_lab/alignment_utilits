#!/usr/bin/env python
"""
  @file		comparison.py
  @brief	Comparison of the values of the alignment parameters
  @author	Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version	$0.01 $
  @date		$Date: 27.10.2016 $
"""

import os, sys, getopt

sys.path.insert(0, 'datparser')

from DatModel import DatDB

dats = []

# tmp/detectors.2016.W07.aloff.ai0.dat.json

args = sys.argv[ 1: ]

if len( args ) < 2:
	raise Exception( 'Too few arguments' )

table = []
last = 0

for num, arg in enumerate( args ):
	row = [ os.path.basename( arg ) ]
	db = DatDB( arg )
	dats.append( db )
	res = db.select( 'det' )
	for dets in res:
			if 'FI15U1__' in dets:
				if num == 0:
					diff = '0'
				else:
					diff = str( abs( last - float( dets[10] ) ) )
				row.extend( [ dets[10], diff ] )
				last = float( dets[10] )
				# 10 means Ycen

	table.append( row )

for row in table:
	print ( '\t'.join( row ) )


	

