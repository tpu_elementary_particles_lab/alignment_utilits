#!/usr/bin/env python
"""
  @file	preparation.py
  @brief   Create the base directories and files for the alignment of the defined RUN
  @author  Alexandr Chumakov <alexandr.achumakov@cern.ch>, Evgenii Levchenko <evgenii.levchenko@cern.ch>
  @version $0.02 $
  @date	$Date: 20.10.2016 $
"""

import os, shutil, io

execfile( 'sets.py' )

def update_options( filepath, changes ):
	print( '> Updating file: ' + filepath )
	
	# open file to read
	text = open( filepath, 'r' )
	output_list = text.read().splitlines()
	text.close()
	
	# open the same file to overwrite
	output = open( filepath, 'w' )
	
	for num, line in enumerate( output_list ):
		for key in changes:
			if key in line:
				output_list[ num ] = line.replace( line, key + ' ' + changes[ key ] )
				print( '"' + key + '" value was found and replaced with ' + changes[ key ] )
		output_list[ num ] += '\n'

	output.writelines( output_list )
	output.close()
	print( '< Updated file: ' + filepath )


try:
	os.mkdir( run_dir )
except OSError as e:
	if e.errno == 17:
		print( 'Run dir already exists' )
except:
	print( e.strerror )
	raise
	
# os.chmod( run_number, 0755 )

for keyname in dirs_convention:
	print( 'Creating directory: ' + dirs_convention[ keyname ] )
	try:
		os.makedirs( run_dir + dirs_convention[ keyname ] )
	except OSError as e:
	  if e.errno == 17:
		  continue
	except:
		print( e.strerror )
		raise


for keyname in base_files:
	print( 'Copying file: ' + run_dir + dirs_convention[ keyname.split( '_' )[0] ] + running_files[ keyname ] )
	shutil.copy( base_files[ keyname ], run_dir + dirs_convention[ keyname.split( '_' )[0] ] + running_files[ keyname ] )


'''
//    ************ MAIN SWITCHES START HERE ************************
detector table /afs/cern.ch/work/a/achumako/alignment/271357/geometry/detectors.2016.W07.aloff.ai0.dat
TraF Dicofit /afs/cern.ch/compass/detector/geometry/2016/dico/dico.269156.mu+
Data file /castor/cern.ch/compass/data/2016/raw/W07/cdr11001-271357.raw
histograms home /afs/cern.ch/work/a/achumako/alignment/271357/traf/output/traf.run271357.cdr11001.ai1.root
TraF iCut [15] -1
'''
changes_traf_opt = {
	'detector table': run_dir + dirs_convention[ 'geometry' ] + running_files[ 'geometry_opt' ],
	'TraF Dicofit': path_to_dico,
	'Data file': '/castor/cern.ch/compass/data/' + run_year + '/raw/W' + run_week + '/cdr11010-' + run_number + '.raw',
	'histograms home': run_dir + dirs_convention[ 'traf_output' ] + 'traf.run' + run_number + '.cdr11010.ai' + iteration + '.root',
	'TraF iCut': '[15] -1' # what does it mean?
}

'''
////////////////////////////////// main switches start here ///////////////////////////////
detector table /afs/cern.ch/work/a/achumako/alignment/271357/geometry/detectors.2016.W07.aloff.ai0.dat
input tree /afs/cern.ch/work/a/achumako/alignment/271357/traf/output/traf.*.ai1.root
output file /afs/cern.ch/work/a/achumako/alignment/271357/align/output/align.W07.ai1.out
'''
changes_align_opt = {
	'detector table': run_dir + dirs_convention[ 'geometry' ] + running_files[ 'geometry_opt' ],
	'input tree': run_dir + dirs_convention[ 'traf_output' ] + 'traf.*.root',
	'output file': run_dir + dirs_convention[ 'align_output' ] + 'align.W' + run_week + '.ai' + iteration + '.out'
}

'''
//////////////////////////////////main switches start here ///////////////////////////////
detector table /afs/cern.ch/work/e/elevchen/alignment/275260/geometry/detectors.2016.W13.aloff.ai2chanc.dat
input tree /afs/cern.ch/work/e/elevchen/alignment/275260/traf/output/traf.alon.*.root
CheckTracks psfile /afs/cern.ch/work/e/elevchen/alignment/275260/tracks/checktracks.W13.ai1.ps
'''
changes_checktracks_opt = {
	'detector table': geometry_updated,
	'input tree': run_dir + dirs_convention[ 'traf_output' ] + 'traf.*.root',
	'CheckTracks psfile': run_dir + dirs_convention[ 'checktracks_output' ] + 'checktracks.W' + run_week + '.ai' + iteration + '.ps'
}

update_options( run_dir + dirs_convention[ 'traf' ] + running_files[ 'traf_opt' ], changes_traf_opt )
update_options( run_dir + dirs_convention[ 'align' ] + running_files[ 'align_opt' ], changes_align_opt )
update_options( run_dir + dirs_convention[ 'checktracks' ] + running_files[ 'checktracks_opt' ], changes_checktracks_opt )


