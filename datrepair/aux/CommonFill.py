#!/usr/bin/env python
"""
  @file		CommonFill.py
  @brief	Writing necessary values from dat file into text files in order to apply them for Adam's ROOT-macros
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.03 $
  @date		$Date: 16.11.2016 $
"""

import os, sys, glob, shutil
import numpy
sys.path.insert(1, '../../datparser')
from DatToDict import parse_geometry


class CommonFill:

	def __init__( self, part, det_tuple, datfiles, outdir ):
		self.station = station
		self.dir = outputdir
		self.skip_flag = 'absent value';

	def createfiles( part, det_tuple, datfiles, outdir ):
		skip_flag = self.skip_flag

		try:
			print('Creating "' + part + '" directory...')
			os.mkdir( outdir + part )
		except OSError as e:
			if e.errno == 17:
				print('    Warning: the directory "' + part + '" already exists!')
				shutil.rmtree( outdir + part )
				print('    Recreating "' + part + '" directory (removing old files and creating new ones).')
				os.mkdir( outdir + part )
			

		# go through dat files and exctract the values
		for line_no, datfile in enumerate( datfiles ):
			
			inputfile = open( datfile, 'r' )
			data = parse_geometry( inputfile, ( 'det' ), True )
			inputfile.close()

			detvalues = data['det']		
			
			zvalues = {}
			for detitem in detvalues:
				dets = detitem['content']
				station = dets[1][:4]
				coord = float( dets[9] )
				if station in zvalues:				
					zvalues[station].append( coord )
				else:
					zvalues[station] = [ coord ]
						
				current_dat = os.path.basename( datfile )
				op_dets = open( outdir + part + '/' + 'detectors.dat', 'a+' )
				if not current_dat in op_dets.read():
					op_dets.write( str( line_no ) + ' ' + current_dat + '\n' )
				op_dets.close()

			for stat in det_tuple:
				op_zcoord = open( outdir + part + '/' + 'ZAverage_' + stat + '.txt', 'a+' )
				if stat in zvalues:
					op_zcoord.write( str( numpy.mean( zvalues[stat] ) ) + '\n' )
				else:
					op_zcoord.write( skip_flag + '\n' )
				op_zcoord.close()	

		print( 'Files have been created and filled' )

	'''
	##### usage #####
	det_stations = ['FI01', 'SI01', 'FI02', 'SI02', 'FI03', 'SI03', 'MP01', 'MP02', 'MP03', 'DC00', 'DC01']
	dir_with_dats = '/home/xan/HEPSoft/COMPASS/align_utils/tmp/comparison/dats_years/'
	datfiles = glob.glob( dir_with_dats + '*.dat' )
	result_dir = '/home/xan/HEPSoft/COMPASS/align_utils/tmp/comparison/zplots_average/'
	'''
	# go ahead !
	#createfiles( 'BTplusTarget', det_stations, sorted( datfiles ), result_dir )

