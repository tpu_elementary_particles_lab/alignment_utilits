#!/usr/bin/env python
"""
  @file		sets.py
  @brief	
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.01 $
  @date		$Date: 01.03.2018 $
"""
import sys, glob, getopt, os

params = {}

# set default rule directory
params[ 'rulfiles' ] = os.path.dirname(os.path.realpath(__file__)) + '/rules/'


# reading arguments
opts, args = getopt.getopt( sys.argv[ 1: ], 'm:i:o:r:', [ 'mode=', 'inputfile=', 'outputfile=', 'rulfiles=' ] )
for opt in opts:
	if opt[0] == '--mode' or opt[0] == '-m':
		params[ 'mode' ] = opt[1]
	elif opt[0] == '--inputfile' or opt[0] == '-i':
		params[ 'inputfile' ] = opt[1]
	elif opt[0] == '--outputfile' or opt[0] == '-o':
		params[ 'outfile' ] = opt[1]
	elif opt[0] == '--rulfiles' or opt[0] == '-r':
		params[ 'rulfiles' ] = opt[1]


# checking for missed arguments
for key in required_params:
	if key not in params:
		params[ key ] = raw_input( 'Please enter the value for necessary parameter "' + key + '" ' )


'''
# checking for the valid mode value
if params[ 'mode' ] not in modes:
	params[ key ] = raw_input( 'Miscorrect mode argument, please type one of "' + modes + '" ' )

'''
