# Scripts for checking and repairing of geometry files

This is a tool for plotting the values of detectors planes inside the detector station. 
It is meant for searching for peculiar behavior due to divergence in the alignment procedure.

## Requirements

The needed version of Python is 2.7.

## Usage
``$ python run.py -m <MODE> -i <INPUT_FILE> -o <OUTPUT_FILE>``

where

* -i path to the .dat file which has to be checked or repaired 
* -o path to the repaired detectors.dat
* -m mode which can be one of the following:

  1. check
  2. dzones
  3. constraints
  
  where 
  check - is just checking the detectors.dat without creating a new one. This mode present all inconsistencies in the terminal output.
  dzones - fixed the positions of deadzones in detectors.dat. Fixed positions will be written in the output detectors.dat (noted in the flag -o).
  constraints - changed values in detectors.dat so the values will satisfy the constraints in the output detectors.dat
  
