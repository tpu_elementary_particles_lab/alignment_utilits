#!/usr/bin/env python
"""
  @file		run.py
  @brief	Executable file for run of the script
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.01 $
  @date		$Date: 01.03.2018 $
"""

from modes import *


factory = {
    'check': CheckDatfile.CheckDatfile,
    'constr': Constraints.Constraints,
    'dzones': DeadZones.DeadZones
    }

'''
def checking(mode):
    mode.checking( names, sorted( datfiles ), params )

def constraints(mode):
    mode.constraints( params, names )

def deadzones(mode):
    mode.deadzones( params, names )

modes = {
    'check':  checking,
    'constr': constraints,
    'dzones': deadzones
}
'''

required_params = ( 'mode', 'inputfile', 'rulfiles' )
execfile( 'aux/sets.py' )

factory[ params[ 'mode' ] ]( params )

"""
mode = factory[ params[ 'mode' ] ]( params[ 'outdir' ] )
steps[ step ]( mode )
"""
