#!/usr/bin/env python
"""
  @file      CheckDatfile.py
  @brief    
  @author    Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version   $0.01 $
  @date      $Date: 01.03.2018 $
"""

import os, sys, glob, shutil, json
import pprint
#import numpy
sys.path.insert(1, '../datparser')
sys.path.insert(0, '..')
from DatToDict import parse_geometry


class CheckDatfile:

    # Constructor of class. Defining necessary attributes and calling methods
    def __init__( self, pars ):
        self.datfile = pars['inputfile']

        # a dictionary of rules from all json files
        self.rules = []
        # go through directory with files describing rules
        rulfiles = sorted( glob.glob( pars[ 'rulfiles' ] + '*.json' ) )
        for rulfile in rulfiles:
            with open(rulfile) as json_file:    
                 ruldict = json.load(json_file)
            self.rules.extend(ruldict)
        # open, parsing into list and close detectors.dat noted in option inputfile
        rfile = open( self.datfile, 'r' )
        self.data = parse_geometry( rfile, ( 'det', 'rot', 'dead' ), True )
        rfile.close()
        
        self.deadzones()
        self.constraints()


    def deadzones( self ):
        pp = pprint.PrettyPrinter(indent=4)
        dzones = self.data['dead']
        
        # build same ID lines
        dd = {}
        for det in self.data['det']:
            for dzone in dzones:
                if dzone['content'][0] == det['content'][0]:
                    dd[dzone['content'][0]] = {'det': det['content'], 'dead': dzone['content'], 'lineno': dzone['lineno']}
        #pp.pprint(dd)
        
        dznames = [item['tbname'] for item in self.rules if item['rule_type'] == 'dzone']

        for k, dzone in dd.items():
                if dzone['det'][1] in dznames: # find dead zone line in detectors.dat which has to be checked
                    for rule in self.rules:
                        if rule['tbname'] == dzone['det'][1]:
                            if 'xcenter' in rule['ratio'] and rule['ratio']['xcenter'] != (float(dzone['det'][10])-float(dzone['dead'][9])):
                                print('DEAD ZONE X center inconsistency for ' +  dzone['det'][1] + ":\t"
                                        +  dzone['det'][10] + " (det)\t - " + dzone['dead'][9] + " (dead)\t != " + str(rule['ratio']['xcenter']) 
                                      )
                            if 'ycenter' in rule['ratio'] and rule['ratio']['ycenter'] != (float(dzone['det'][11])-float(dzone['dead'][10])):
                                print('DEAD ZONE Y center inconsistency for ' +  dzone['det'][1] + ":\t"
                                        +  dzone['det'][11] + " (det)\t - " + dzone['dead'][10] + " (dead)\t != " + str(rule['ratio']['ycenter']) 
                                      )
                            if 'zcenter' in rule['ratio'] and rule['ratio']['zcenter'] != (float(dzone['det'][9])-float(dzone['dead'][8])):
                                print('DEAD ZONE Z center inconsistency for ' +  dzone['det'][1] + ":\t"
                                        +  dzone['det'][9] + " (det)\t - " + dzone['dead'][8] + " (dead)\t != " + str(rule['ratio']['zcenter']) 
                                      )

    def constraints( self ):
        #pp = pprint.PrettyPrinter(indent=4)

        index = {'xcenter': 10, 'ycenter': 11, 'zcoord': 9, 'angle': 14, 'pitch': 13}

        cd = {}
        proj2nd = False
        for det in self.data['det']:
            #TODO: handle with pixel GP and MP, which have non-unique TBName
            if det['content'][1][4:5] == 'M' or det['content'][1][4:5] == 'P':
                if proj2nd == False:
                    proj2nd = True
                    tbname = det['content'][1][:7] + '_U'
                else:
                    tbname = det['content'][1][:7] + '_V'
            else:
                tbname = det['content'][1]
            cd[tbname] = {'det': det['content'], 'lineno': det['lineno'] }


        for item in self.rules:
            if item['rule_type'] == 'constraint':
                res = 0.0
                ctext = ''
                for k, term in item['coeffs'].items():
                    res = res + term*float(cd[k]['det'][index[item['param']]])
                    if ctext:
                        ctext = ctext + ' & '
                    ctext = ctext + k
                    #print('Calc for ' + k + ' is ' + str(res) + ' with term ' + str(term*float(cd[k]['det'][index[item['param']]])) )
                if item['rms'] != res:
                    print('Constraint between ' + ctext + ' by parameter ' + item['param'] + ' is not hold')


    
    def nominalbias( self ):
        print('not yet implemented, there is only such idea')
        
