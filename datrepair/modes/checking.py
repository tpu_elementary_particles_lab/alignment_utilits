#!/usr/bin/env python
"""
  @file		Xcoord.py
  @brief	Writing necessary values from dat file into text files in order to apply them for Adam's ROOT-macros
  @author	Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version	$0.03 $
  @date		$Date: 01.12.2016 $
"""

import os, sys, glob, shutil
import numpy
sys.path.insert(1, '../datparser')
sys.path.insert(0, '..')
from DatToDict import parse_geometry


class CheckDatfile:

	def __init__( self, inputfile ):
		self.datfile = inputfile
		

	def preparation( self, names, datfiles, params ):
		names = names['all']
		for det in names:
			try:
				shutil.rmtree( self.dir + det )
			except:
				print('Creating "' + det + '" directory')
			os.mkdir( self.dir + det )
			os.mkdir( self.dir + det + '/macro' )
			os.mkdir( self.dir + det + '/image' )
			os.mkdir( self.dir + det + '/value' )
			os.mkdir( self.dir + det + '/root' )

		# go through dat files and exctract the values
		for line_no, datfile in enumerate( datfiles ):
			
			inputfile = open( datfile, 'r' )
			data = parse_geometry( inputfile, ( 'det', 'rot' ), True )
			inputfile.close()

			detvalues = data['det']

			for detitem in detvalues:
				dets = detitem['content']
				# print( dets[1][:4] ) # e.g. FI15U1__
				if dets[1][:4] in names: # e.g. FI15U1__ 
					unique_name = dets[1] + '-' + dets[2]		
					current_dat = os.path.basename( datfile )

					op_dets = open( self.dir + dets[1][:4] + '/value/' + 'detectors.dat', 'a+' )
					if not current_dat in op_dets.read():
						op_dets.write( str( line_no ) + ' ' + current_dat + '\n' )
					op_dets.close()

					op_planes = open( self.dir + dets[1][:4] + '/value/' + 'DetectorNames.txt', 'a+' )
					op_xcoord = open( self.dir + dets[1][:4] + '/value/' + 'Xcoord_' + unique_name + '.txt', 'a+' )

					if not unique_name in op_planes.read():
						op_planes.write( unique_name + '\n' )
						skip_corrections = [ self.skip_flag for x in range( 0, line_no ) ]
					else:
						file_len = len( op_xcoord.readlines() )
						skip_corrections = [ self.skip_flag for x in range( file_len, line_no ) ]

					if len( skip_corrections ) > 0:
						op_xcoord.write( '\n'.join( skip_corrections ) + '\n' + dets[14] + '\n' )
					else:
						op_xcoord.write( dets[10] + '\n' )
						
					op_planes.close()
					op_xcoord.close()
		print( 'Files have been created and filled' )


	def showresults( self, params, names ):
		root.gROOT.SetBatch( False )
		root.gROOT.LoadMacro('./root_macros/ShowResults.c')
		root.ShowResults( params[ 'outdir' ], "Xcoord" )
