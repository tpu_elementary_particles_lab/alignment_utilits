#!/usr/bin/env python
"""
  @file      Constraints.py
  @brief    
  @author    Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version   $0.01 $
  @date      $Date: 01.03.2018 $
"""

import os, sys, glob, shutil, json
import pprint
import copy
from tabulate import tabulate

#import numpy
sys.path.insert(1, '../datparser')
sys.path.insert(1, '../model')
sys.path.insert(0, '..')
from Parser import parse_geometry
from DBManager import DBManager
from Descriptor import Descriptor


class Constraints:

    # Constructor of class. Defining necessary attributes and calling methods
    def __init__( self, pars ):
        self.datfile = pars['inputfile']
        self.outfile = pars['outfile']
        # a dictionary of rules from all json files
        self.rules = []
        # go through directory with files describing rules
        rulfiles = sorted( glob.glob( pars[ 'rulfiles' ] + '*.json' ) )
        for rulfile in rulfiles:
            with open(rulfile) as json_file:    
                 ruldict = json.load(json_file)
                 for rul in ruldict:
                     if type(rul) is dict and 'param' in rul:
                         if rul['param'] == 'ucoord':
                             add = dict(rul)
                             rul['param'] = 'xcenter'
                             add['param'] = 'ycenter'
                             ruldict.append(add)
            self.rules.extend(ruldict)
            
        descr = Descriptor()
        struct = descr.struct
        # open, parsing into list and close detectors.dat noted in option inputfile
        rfile = open( self.datfile, 'r' )
        self.data = parse_geometry(rfile, tuple([x for x, v in struct.items()]))
        rfile.close()
        
        #self.constraints()
        self.constraints_via_db()

    # DON'T FORGET to update deadzones when it updates the detector positions
    def constraints( self ):
        pp = pprint.PrettyPrinter(indent=4)
        dzones = self.data['dead']
        
        index = {'xcenter': 10, 'ycenter': 11, 'zcoord': 9, 'angle': 14, 'pitch': 13}
        
        # building a convinient dictionary to handle detectors.dat
        # structure of the dict {'ln_N': {'content': ..., 'type': ..., 'tbname': ..., 'dz_ln': ...}}
        # where: ln_N corresponds to line number, "ln_" is prefix and "N" is the proper number of line.
        #        each ln_N contains subdictionary:
        #        'content' - a list from self.data['content']
        #        'type' - type of line: det, dead, rot, mag, comment or something else
        #        'tbname' (optional) contains TBName if the line is the det or dead type
        #        'dz_ln'  (optional) if it's a det type line and there is a corresponding dead zone, it is convinient to save the link to the line number of the dead zone description
        conv = {}
        
        # a flag to pick up the pixel parts. Same approach which is used in CORAL alignment
        # HERE IT DOESN'T WORK SINCE PYTHON DICTIONARIES ARE NOT ORDERED
        proj2nd = False


        for typeof, item in self.data.items():
            for det in item:
                if typeof == 'det':
                    # TODO: check it! ==> look at the 60th line !!!
                    if det['content'][1][4:5] == 'M' or det['content'][1][4:5] == 'P':
                        if proj2nd == False:
                            proj2nd = True
                            tbname = det['content'][1][:6] + '_U'
                        else:
                            tbname = det['content'][1][:6] + '_V'
                    else:
                        tbname = det['content'][1]
                        proj2nd = False
                    for dzone in dzones:
                        # if IDs of detectors match in dead and det sector of detectors.dat
                        if dzone['content'][0] == det['content'][0]:
                            conv['ln_' + str(det['lineno'])] = {'content': det['content'], 'type': typeof, 'tbname': tbname, 'dz_ln': dzone['lineno'], 'lineno': det['lineno']}
                        else:
                            conv['ln_' + str(det['lineno'])] = {'content': det['content'], 'type': typeof, 'tbname': tbname, 'lineno': det['lineno']}
                else:
                    conv['ln_' + str(det['lineno'])] = {'content': det['content'], 'type': typeof, 'tbname': None, 'lineno': det['lineno']}
        # end of building a convinient dictionary
        
        
        handledat = conv.copy()

        for item in self.rules:
            if item['rule_type'] == 'constraint':
                res = 0.0
                ctext = ''
                for k, term in item['coeffs'].items():
                    ln = self.findTBname(k, conv)
                    if ln:
                        line = conv['ln_' + ln]
                        res = res + term*float(line['content'][index[item['param']]])
                        if ctext:
                            ctext = ctext + ' & '
                        ctext = ctext + k

                if item['rms'] != res:
                    diff = res - item['rms']
                    correction = diff/len(item['coeffs'])
                    for k, term in item['coeffs'].items():
                        ln = self.findTBname(k, conv)
                        if ln:
                            line = conv['ln_' + ln]
                            new_det_val = float(line['content'][index[item['param']]]) - term*correction
                            print('Correction for ' + k + ' is equal ' + str(term*correction) + '. So it became ' + str(new_det_val) + ' It was ' + str(line['content'][index[item['param']]]))
                            handledat['ln_' + ln]['content'][index[item['param']]] = new_det_val
                            if 'ln_dz' in line:
                                new_dead_val = float(conv['ln_' + conv['ln_' + ln]['ln_dz']][index[item['param']] - 1]) - term*correction
                                handledat['ln_' + conv['ln_' + ln]['ln_dz']][index[item['param']] - 1] = new_dead_val

        
        sortedlist = []
        wf = open( self.outfile, 'w' )

        for i in range(0, len(handledat)-1):
            value = handledat['ln_'+str(i)]
            if type( value['content'] ) is list:
                content = ' ' + value['type'] + '   '
                for c in value['content']:
                    content += str( c ) + "   "
                content += '\n'
            else:
                content = value[ 'content' ]
            sortedlist.append( content )

        wf.writelines( sortedlist )
        wf.close()


    def findTBname(self, value, dictionary):
        for k, v in dictionary.iteritems():
            # check output for pixel part buisness
            '''if v['tbname']:
                if v['tbname'][:2]=='GP':
                    print(v['tbname'] + ' line ', v['lineno'])'''
            if v['tbname'] == value:
                return str(v['lineno'])
        return

    def constraints_via_db( self ):
        datlines = self.data
        db = DBManager()
        descr = Descriptor()
        struct = descr.struct
        data = {}
        
        column = {'xcenter': 'x_center', 'ycenter': 'y_center', 'zcoord': 'z_center', 'angle': 'angle', 'pitch': 'pitch'}
        
        proj2nd = False
        for k, v in datlines.items():
            if k in struct:
                mapping = descr.conformity[k]
                data[struct[k]] = []
                for lines in v:
                    linedict = {'line_no': lines['lineno'], 'file_id': 1}
                    if k == 'det' or k == 'dead':
                        tbname = lines['content'][mapping['tbname']]
                        linedict['unique_key'] = lines['content'][mapping['det_id']]+'-'+tbname+'-'+lines['content'][mapping['det']]
                        if (tbname[4:5] == 'M' or tbname[4:5] == 'P') and tbname[:2] != 'BM':
                            if proj2nd == False:
                                proj2nd = True
                                tbname_proj = tbname[:6] + '_U'
                            else:
                                proj2nd = False
                                tbname_proj = tbname[:6] + '_V'
                        else:
                            tbname_proj = tbname
                            proj2nd = False
                        linedict['tbname_proj'] = tbname_proj
                    for col, index in mapping.items():
                        if not index > len(lines['content'])-1:
                            #print('index '+ str(index))
                            #print('length '+ str(len(lines['content'])))
                            linedict.update({col: lines['content'][index]})
                                #print(line)
                                #exit
                    data[struct[k]].append(linedict)
            else:
                #print('current key! ' + str(len(v)))
                data['auxiliaries'] =  []
                for lines in v:
                    data['auxiliaries'].append({'line_no': lines['lineno'], 'file_id': 1, 'content': lines['content']})
        db.datinput(data)

        cur = db.connection.cursor()
        print(self.rules)
        #return 0
        for item in self.rules:
            print(item)
            if item['rule_type'] == 'constraint':
                res = 0.0
                ctext = ''
                for k, term in item['coeffs'].items():
                    cur.execute("SELECT `"+column[item['param']]+"` FROM `datfile_detectors` WHERE `tbname_proj`='" +k+ "'")
                    value = cur.fetchone()[0]
                    #print('In DB the following value was found: ' + str(value))
                    #ln = self.findTBname(k, conv)
                    if value:
                        #line = conv['ln_' + ln]
                        res = res + term*float(value)
                        if ctext:
                            ctext = ctext + ' & '
                        ctext = ctext + k
        


                if item['rms'] != res:
                    diff = res - item['rms']
                    correction = diff/len(item['coeffs'])
                    for k, term in item['coeffs'].items():
                        # k - constraint TBName
                        #print("changes for "+ k + " corr - " + str(term*correction));
                        add = -1*term*correction
                        #print('Correction for ' + k + ' is equal ' + str(term*correction) + '. So it became ' + str(new_det_val) + ' It was ' + str(line['content'][index[item['param']]]))
                        cur.execute("UPDATE `datfile_detectors`  SET `"+column[item['param']]+"`=`"+column[item['param']]+"`+"+str(add)+" WHERE `tbname_proj`='"+k+"'")
                        #cur.execute("UPDATE `datfile_dead_zones` SET column=column+term*correction WHERE tbname=k")
        
        db.connection.commit()
        
        db.datoutput(1, 'foo.dat')
        
        db.connection.close()

