#!/usr/bin/env python
"""
  @file      CheckDatfile.py
  @brief    
  @author    Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version   $0.01 $
  @date      $Date: 01.03.2018 $
"""

import os, sys, glob, shutil, json
import pprint
#import numpy
sys.path.insert(1, '../datparser')
sys.path.insert(0, '..')
from DatToDict import parse_geometry
from tabulate import tabulate

class DeadZones:

    # Constructor of class. Defining necessary attributes and calling methods
    def __init__( self, pars ):
        self.datfile = pars['inputfile']
        self.outfile = pars['outfile']
        # a dictionary of rules from all json files
        self.rules = []
        # go through directory with files describing rules
        rulfiles = sorted( glob.glob( pars[ 'rulfiles' ] + '*.json' ) )
        for rulfile in rulfiles:
            with open(rulfile) as json_file:    
                 ruldict = json.load(json_file)
            self.rules.extend(ruldict)
        # open, parsing into list and close detectors.dat noted in option inputfile
        rfile = open( self.datfile, 'r' )
        self.data = parse_geometry( rfile, ( 'det', 'rot', 'dead' ), False )
        rfile.close()
        
        self.deadzones()


    def deadzones( self ):
        pp = pprint.PrettyPrinter(indent=4)
        dzones = self.data['dead']
        
        # build same ID lines
        dd = {}
        for det in self.data['det']:
            for dzone in dzones:
                if dzone['content'][0] == det['content'][0]:
                    dd[dzone['content'][0]] = {'det': det['content'], 'dead': dzone['content'], 'lineno': dzone['lineno']}
        #pp.pprint(dd)
        
        dznames = [item['tbname'] for item in self.rules if item['rule_type'] == 'dzone']

        handledat = self.data.copy()
        for k, dzone in dd.items():
                if dzone['det'][1] in dznames: # find dead zone line in detectors.dat which has to be checked
                    for rule in self.rules:
                        if rule['tbname'] == dzone['det'][1]:
                            if rule['ratio']['xcenter'] != (float(dzone['det'][10])-float(dzone['dead'][9])):
                                for i, odz in enumerate( self.data['dead'] ):
                                    if odz['lineno'] == dzone['lineno']:
                                        
                                        print('Fixed DEAD ZONE X center inconsistency for ' +  dzone['det'][1] + ", that was:\t"
                                              + dzone['det'][10] + " (det)\t - " + dzone['dead'][9] + " (dead)\t != " + str(rule['ratio']['xcenter']) 
                                             )
                                        handledat['dead'][i]['content'][9] = dzone['det'][10]
                            if rule['ratio']['ycenter'] != (float(dzone['det'][11])-float(dzone['dead'][10])):
                                for i, odz in enumerate( self.data['dead'] ):
                                    if odz['lineno'] == dzone['lineno']:
                                        print('Fixed DEAD ZONE Y center inconsistency for ' +  dzone['det'][1] + ", that was:\t"
                                              +  dzone['det'][11] + " (det)\t - " + dzone['dead'][10] + " (dead)\t != " + str(rule['ratio']['ycenter']) 
                                             )
                                        handledat['dead'][i]['content'][10] = dzone['det'][11]
                            if rule['ratio']['zcenter'] != (float(dzone['det'][9])-float(dzone['dead'][8])):
                                for i, odz in enumerate( self.data['dead'] ):
                                    if odz['lineno'] == dzone['lineno']:
                                        print('Fixed DEAD ZONE Y center inconsistency for ' +  dzone['det'][1] + ", that was:\t"
                                              +  dzone['det'][9] + " (det)\t - " + dzone['dead'][8] + " (dead)\t != " + str(rule['ratio']['zcenter']) 
                                             )
                                        handledat['dead'][i]['content'][8] = dzone['det'][9]

        sortedlist = []
        wf = open( self.outfile, 'w' )

        for key, value in handledat.items():
            for b in value:

                if type( b['content'] ) is list:
                    content = ' ' + key + '   '
                    for c in b['content']:
                        content += str( c ) + "   "
                    content += '\n'
                    #content = tabulate( content )
                else:
                    content = b[ 'content' ]
                sortedlist.insert( int( b['lineno'] ), content )

        wf.writelines( sortedlist )
        wf.close()
