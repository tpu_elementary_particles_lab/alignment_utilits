#!/usr/bin/env python
"""
  @file		DatModel.py
  @brief	Model to interact with database
  @author	Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version $0.02 $
  @date	$Date: 26.10.2016 $
"""

import tinydb

class DatDB:

	def __init__( self, filename = None ):
		self.db = tinydb.TinyDB( filename )

	def save( self, data ):
		self.db.insert( data )

	def read( self, query ):
		result = self.db.search( query )
		return result

	def getall( self ):
		result = self.db.all( )
		return result

	def select( self, det ):
		result = []
		sel = self.getall()[0][ det ]
		# maybe I don't understand tinydb, but I am forced to regroup the response and it makes me upset
		for item in sel:
			result.append( item['content'] )
		return result
		
	def getrotmatr( self, num ):
		sel = self.getall()[0]['rot']
		# looking for rot matrix with ID (that's item[0]) = num
		for item in sel:
			#print( item['content'][0] + ' = ' + str(num) )
			if item['content'][0] == num:
				return item['content']
		print(' Not found corresponding rot matrix! ')
			
		
		
