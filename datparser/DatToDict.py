#!/usr/bin/env python
"""
  @file	DatToDict.py
  @brief   Transform from .dat to python dictionaries
  @author  Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version $0.02 $
  @date	$Date: 13.10.2016 $
"""

import re

#def parts( ):
#	return ('mag', 'rot', 'det', 'smtx', 'rich', 'rich1', 'phot0', 'cath', 'mirr', 'mirr0', 'bbp', 'dead')


def parse_geometry( source,  # file or tuple of lines
					prefixes,  # tuple of strings denoting first token in interesting lines
					keepAuxInfo=False  # Wheter do keep non-prefixed information
				   ):
	# define dict of results					   
	results = { '@auxinfo' : [] }				   
					   
	for num, line in enumerate( source ):
		
		# find all matches which are not spaces and tabs
		values = re.findall( '([^\s^\t]+)', line )
		length = len( values )
		
		# the first match should be the prefix name, so check the condition
		if length > 0 and values[0] in prefixes:
			
			# variable which is responsible for structure saving in DB 
			item = { "lineno" : num, "content" : [ values[ i ] for i in range( 1, length ) ] }
			
			if values[0] in results:				
				results[ values[0] ].append( item )
			else:
				results[ values[0] ] = [ item ]
			
		elif keepAuxInfo is False:
			results[ '@auxinfo' ].append( { "lineno" : num, "content" : line } )
	
	return results
	


