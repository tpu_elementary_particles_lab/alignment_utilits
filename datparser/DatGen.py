#!/usr/bin/env python
"""
  @file	DatGen.py
  @brief   Generate .dat from DB data
  @author  Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version $0.02 $
  @date	$Date: 13.10.2016 $
"""

from DatModel import DatDB

from tinydb import Query

def generate( writefile ):
	sortedlist = []

	db = DatDB( writefile + '.json' )
	data = db.getall( )
	wf = open( writefile, 'w' )

	for a in data[ 0 ]:
		for b in data[ 0 ][ a ]:

			if type( b[ 'content' ] ) is list:
				content = a + '   '
				for c in b[ 'content' ][ 0 ]:
					content += str( c ) + '   '
				content += '\n'
			else:
				content = b[ 'content' ]
			sortedlist.insert( int( b[ 'lineno' ] ), content )

	for line in sortedlist:
		wf.write( str( line ) )

	wf.close()

