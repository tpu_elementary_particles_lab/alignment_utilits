# Geometry files comparison tool

This is a tool for plotting the values of detectors planes inside the detector station. 
It is meant for searching for peculiar behavior due to divergence in the alignment procedure.

## Requirements
The tool uses ROOT with pyROOT (a Python extension module). 
Usually PyROOT is enabled by default in the ROOT package. 
But be sure that the PYTHONPATH variable has necessary value in your environment, 
e.g. for bash it should be like this

``export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH``

The needed version of Python is 2.7.

## Usage
1. Put into some folder all dat files which you want to comapare on the same plot. 
ATTENTION: it is important how to name the dat files, since the tool reads the name and extract necessary information (year and kind of physics) from it. 
Specifically, the tool cuts the name on parts separated by point. 
So, e.g., for detectors.2012.108966.mu-.dat the second and fourth parts correspond to year and physics respectively. 
Please, follow this way, in order to get right labels on the comparison plots and to avoid crashes.
2. Make a directory for output files which will be created by tool.
3. Run the tool:
* for preparing of the necessary files (1st step).
* for creating root-files and drawing of plots (2nd step).
* to see the results (3rd step).

All steps of Run can be produced by the run.py file. Syntax is the following


``$ python run.py -i <PATH_TO_FOLDER_WITH_DAT_FILES> -o <PATH_TO_OUTPUT_RESULTS> -m <MODE> -p <PART_OF_SETUP> -s <STEP>``

where

* -i path to the folder with .dat files
* -o path to the result folder
* -m define what values are needed to compare. Now there are modes:

  1. angle
  2. zaverage
  3. zcoord
  4. xcoord
  5. zcoord
  
* -p specific flag for average values that indicates detectors from which part of setup it's needed to represent on the plot. There are three parts:
 
  1. BTplusTarget
  2. SM1_SM2
  3. SM2_plus
  
* -s this flag is up for Run step. The available options are:

  1. pre
  2. draw
  3. show
  
## Examples

Let

``~/geofiles/datfiles``

is the folder with .dat files (the value for -i flag, mentioned in Usage part) and

``geofiles/comparison``

is the path to the result folder  (the value for -o flag, mentioned also above).

Then typical commands to get results, e.g. for "TBplusTarget" area, look the following way:

``$ python run.py -i ~/geofiles/datfiles -o ~/geofiles/comparison -m angle -s pre``

``$ python run.py -i ~/geofiles/datfiles -o ~/geofiles/comparison -m angle -s draw``

``$ python run.py -i ~/geofiles/datfiles -o ~/geofiles/comparison -m angle -s show``



## For developers

Some documentation for developers will be added in the future

