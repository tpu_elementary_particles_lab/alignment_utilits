#!/usr/bin/env python
"""
  @file		pre.py
  @brief	
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.02 $
  @date		$Date: 22.11.2016 $
"""
import ROOT as root

def preparation(mode):
	mode.preparation( names, sorted( datfiles ), params )

def plotting(mode):
	mode.plotting( params, names )

def showresults(mode):
	mode.showresults( params, names )
	raw_input('Type "Enter" to finish')

steps = {
	'pre': preparation,
	'draw': plotting,
	'show': showresults
}

required_params = ( 'mode', 'inputdir', 'outdir' )
execfile( 'aux/sets.py' )
mode = factory[ params[ 'mode' ] ]( params[ 'outdir' ] )
steps[ step ]( mode )
