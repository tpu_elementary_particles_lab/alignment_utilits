#!/usr/bin/env python
"""
  @file		XvsZ.py
  @brief	
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.02 $
  @date		$Date: 10.09.2017 $
"""
import os, sys, glob, shutil
import numpy
import ROOT as root
sys.path.insert(0, '/home/xan/HEPSoft/COMPASS/align_utils/datparser')
from DatToDict import parse_geometry

files = ['/home/xan/HEPSoft/COMPASS/align_utils/tmp/versusZ/detectors.275031.mu+.dat',
		'/home/xan/HEPSoft/COMPASS/align_utils/tmp/versusZ/detectors.108477.mu+.dat']

hs = root.THStack( 'hs', 'stack' )

for datfile in files:
	inputfile = open( datfile, 'r' )
	data = parse_geometry( inputfile, ( 'det' ), True )
	inputfile.close()

	detvalues = data['det']
	zvalues = sorted( [ ( x['content'][1], float(x['content'][9]), float(x['content'][11]) ) for x in detvalues ] )

	ordered_z = sorted( zvalues, key=lambda col: col[1])

	bins = len( detvalues )

	root.gStyle.SetOptStat(11)

	#his = root.TH1D( 'run 275031, 2016', 'Y versus Z', bins, 0, float(bins) )
	his = root.TH1D( 'run 108477, 2012', 'Y versus Z', bins, 0, float(bins) )


	for i, val in enumerate( ordered_z ):
		# X is dets[10]
		# Z is dets[9]
		#hxz.Fill( float( dets[9] ), float ( dets[10] ) )
		his.SetBinContent(i+1, float( val[2] ) )
		his.SetBinError(i+1, 0.000001)


	
	
	hs.Add( his )



for i in range(bins):
	his.GetXaxis().SetBinLabel(i+1, 'Z=' + "{0:.2f}".format(val[1]) + ' ' + val[0] )


hs.GetXaxis().LabelsOption("v")
hs.GetYaxis().SetTitle( 'Ycenter [cm]' )

raw_input('Type "Enter" to finish')
