#!/usr/bin/env python
"""
  @file		sets.py
  @brief	Run comparison of a set of geometry files
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.02 $
  @date		$Date: 22.11.2016 $
"""
import sys, glob, getopt
from modes import *

modes = ( 'zaverage', 'angle', 'zcoord', 'xcoord', 'ycoord' ) # TODO
averages = ( 'zaverage' ) # TODO

params = {}

def parse_config( configfile ):
	with open( configfile, 'r' ) as stream:
		lines = stream.read().splitlines()
		names = filter(None, [ l.split('#')[0].strip() for l in lines ] )
	return names

# reading arguments
opts, args = getopt.getopt( sys.argv[ 1: ], 'm:i:o:p:c:s:', [ 'mode=', 'inputdir=', 'outputdir=', 'part=', 'config=', 'step=' ] )
for opt in opts:
	if opt[0] == '--mode' or opt[0] == '-m':
		params[ 'mode' ] = opt[1]
	elif opt[0] == '--inputdir' or opt[0] == '-i':
		params[ 'inputdir' ] = opt[1] + '/'
	elif opt[0] == '--outputdir' or opt[0] == '-o':
		params[ 'outdir' ] = opt[1] + '/'
	elif opt[0] == '--part' or opt[0] == '-p':
		params[ 'part' ] = opt[1]
	elif opt[0] == '--config' or opt[0] == '-c':
		configfile = opt[1]
	elif opt[0] == '--step' or opt[0] == '-s':
		step = opt[1]

# checking for missed arguments
for key in required_params:
	if key not in params:
		params[ key ] = raw_input( 'Please enter the value for necessary parameter "' + key + '" ' )

# checking for the valid mode value
if params[ 'mode' ] not in modes:
	params[ key ] = raw_input( 'Miscorrect mode argument, please type one of "' + modes + '" ' )
	
names = {}
if params[ 'mode' ] in averages:
	names['BTplusTarget'] = parse_config( '../base/configs/setup/BTplusTarget.conf' )
	names['SM1_SM2'] = parse_config( '../base/configs/setup/SM1_SM2.conf' )
	names['SM2_plus'] = parse_config( '../base/configs/setup/SM2_plus.conf' )
else:
	names['all'] = parse_config( '../base/configs/setup/allStations.conf' )

if not 'part' in params:
	params[ 'part' ] = 'all'


datfiles = sorted( glob.glob( params[ 'inputdir' ] + '*.dat' ) )

factory = {				# TODO
			'zaverage': ZAverage.ZAverage,
			'angle': Angle.Angle,
			'zcoord': Zcoord.Zcoord,
			'xcoord': Xcoord.Xcoord,
			'ycoord': Ycoord.Ycoord
		  }

