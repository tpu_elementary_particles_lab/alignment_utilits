#!/usr/bin/env python
"""
  @file		ZAverage.py
  @brief	Writing necessary values from dat file into text files in order to apply them for Adam's ROOT-macros
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version	$0.03 $
  @date		$Date: 16.11.2016 $
"""

import os, sys, glob, shutil
import numpy
sys.path.insert(1, '../datparser')
sys.path.insert(1, 'root_macros')
sys.path.insert(0, '..')
from DatToDict import parse_geometry
from PlotsCreate import PlotsCreate
#import CommonFill
import ROOT as root

class ZAverage:

	# def __init__( self, part, det_tuple, datfiles, outdir ):
	def __init__( self, outputdir ):
		self.dir = outputdir
		self.skip_flag = 'absent value'

	# 'BTplusTarget', det_stations, sorted( datfiles ), result_dir
	def preparation( self, names, datfiles, params ):
		if params[ 'part' ] != 'all':
			self.createfiles( params[ 'part' ], names[ params[ 'part' ] ], datfiles, self.dir )
		else:
			self.createfiles( 'BTplusTarget', names['BTplusTarget'], datfiles, self.dir )
			self.createfiles( 'SM1_SM2', names['SM1_SM2'], datfiles, self.dir )
			self.createfiles( 'SM2_plus', names['SM2_plus'], datfiles, self.dir )


	def plotting( self, params, names ):
		rmacro = PlotsCreate( 'ZAverage', params[ 'outdir' ] + params[ 'part' ] + '/', names[ params[ 'part' ] ] )
		if params[ 'part'] != 'all':
			for det in names[ params[ 'part' ] ]:
				rmacro.fillRootFile( det )
			rmacro.drawAllStations( 'ZAverage', params[ 'part' ], 'Z average' )
		else:
			for det in names['BTplusTarget']:
				root.DrawDetZAverage( det, params[ 'outdir' ] + 'BTplusTarget' )
			root.DrawAllDetectorStations( params[ 'outdir' ] + 'BTplusTarget' )
			for det in names['SM1_SM2']:
				root.DrawDetZAverage( det, params[ 'outdir' ] + 'SM1_SM2' )
			root.DrawAllDetectorStations( params[ 'outdir' ] + 'SM1_SM2' )
			for det in names['SM2_plus']:
				root.DrawDetZAverage( det, params[ 'outdir' ] + 'SM2_plus' )
			root.DrawAllDetectorStations( params[ 'outdir' ] + 'SM2_plus' )


	def createfiles( self, part, det_tuple, datfiles, outdir ):
		try:
			print('Creating "' + part + '" directory...')
			os.mkdir( outdir + part )
			os.mkdir( outdir + part + '/macro' )
			os.mkdir( outdir + part + '/image' )
			os.mkdir( outdir + part + '/value' )
			os.mkdir( outdir + part + '/root' )

		except OSError as e:
			if e.errno == 17:
				print('    Warning: the directory "' + part + '" already exists!')
				shutil.rmtree( outdir + part )
				print('    Recreating "' + part + '" directory (removing old files and creating new ones).')
				os.mkdir( outdir + part )
				os.mkdir( outdir + part + '/macro' )
				os.mkdir( outdir + part + '/image' )
				os.mkdir( outdir + part + '/value' )
				os.mkdir( outdir + part + '/root' )

		# go through dat files and exctract the values
		for line_no, datfile in enumerate( datfiles ):
			
			inputfile = open( datfile, 'r' )
			data = parse_geometry( inputfile, ( 'det' ), True )
			inputfile.close()

			detvalues = data['det']		
			
			zvalues = {}
			for detitem in detvalues:
				dets = detitem['content']
				station = dets[1][:4]
				coord = float( dets[9] )
				if station in zvalues:				
					zvalues[station].append( coord )
				else:
					zvalues[station] = [ coord ]
						
				current_dat = os.path.basename( datfile )
				op_dets = open( outdir + part + '/value/' + 'detectors.dat', 'a+' )
				if not current_dat in op_dets.read():
					op_dets.write( str( line_no ) + ' ' + current_dat + '\n' )
				op_dets.close()


			for stat in det_tuple:
				op_zcoord = open( outdir + part + '/value/' + 'ZAverage_' + stat + '.txt', 'a+' )
				op_names = open( outdir + part + '/value/' + 'DetectorNames.txt', 'a+' )
				if stat in zvalues:
					op_zcoord.write( str( numpy.mean( zvalues[stat] ) ) + '\n' )
					if not stat in op_names.read():
						op_names.write( stat + '\n' )
				else:
					op_zcoord.write( self.skip_flag + '\n' )
				op_zcoord.close()
				op_names.close()

		print( 'Files have been created and filled' )
		
	def showresults( self, params, names ):
		root.gROOT.SetBatch( False )
		if params[ 'part'] != 'all':
			root.gROOT.LoadMacro( self.dir + params[ 'part'] + '/macro/' + params[ 'part' ] + '_ZAverages.C' )
			getattr( root, params[ 'part' ] + '_ZAverages')()
		else:
			print( 'Need to define a part of the setup' )
