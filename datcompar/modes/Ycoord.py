#!/usr/bin/env python
"""
  @file        Ycoord.py
  @brief    Writing necessary values from dat file into text files in order to apply them for Adam's ROOT-macros
  @author    Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version    $0.03 $
  @date        $Date: 01.12.2016 $
"""

import os, sys, glob, shutil
import numpy
sys.path.insert(1, '../datparser')
sys.path.insert(1, 'root_macros')
sys.path.insert(0, '..')
from DatToDict import parse_geometry
from PlotsCreate import PlotsCreate
#import CommonFill
import ROOT as root

class Ycoord:

    # def __init__( self, part, det_tuple, datfiles, outdir ):
    def __init__( self, outputdir ):
        self.dir = outputdir
        self.skip_flag = 'absent value'

    #def preparation( det_tuple, datfiles, outdir ):
    def preparation( self, names, datfiles, params ):
        names = names['all']
        for det in names:
            try:
                shutil.rmtree( self.dir + det )
            except:
                print('Creating "' + det + '" directory')
            os.mkdir( self.dir + det )
            os.mkdir( self.dir + det + '/macro' )
            os.mkdir( self.dir + det + '/image' )
            os.mkdir( self.dir + det + '/value' )
            os.mkdir( self.dir + det + '/root' )

        # go through dat files and exctract the values
        for line_no, datfile in enumerate( datfiles ):
            
            inputfile = open( datfile, 'r' )
            data = parse_geometry( inputfile, ( 'det', 'rot' ), True )
            inputfile.close()

            detvalues = data['det']

            for detitem in detvalues:
                dets = detitem['content']
                # print( dets[1][:4] ) # e.g. FI15U1__
                if dets[1][:4] in names: # e.g. FI15U1__ 
                    unique_name = dets[1] + '-' + dets[2]        
                    current_dat = os.path.basename( datfile )

                    op_dets = open( self.dir + dets[1][:4] + '/value/' + 'detectors.dat', 'a+' )
                    if not current_dat in op_dets.read():
                        op_dets.write( str( line_no ) + ' ' + current_dat + '\n' )
                    op_dets.close()

                    op_planes = open( self.dir + dets[1][:4] + '/value/' + 'DetectorNames.txt', 'a+' )
                    op_ycoord = open( self.dir + dets[1][:4] + '/value/' + 'Ycoord_' + unique_name + '.txt', 'a+' )

                    if not unique_name in op_planes.read():
                        op_planes.write( unique_name + '\n' )
                        skip_corrections = [ self.skip_flag for x in range( 0, line_no ) ]
                    else:
                        file_len = len( op_ycoord.readlines() )
                        skip_corrections = [ self.skip_flag for x in range( file_len, line_no ) ]

                    if len( skip_corrections ) > 0:
                        op_ycoord.write( '\n'.join( skip_corrections ) + '\n' )
                    op_ycoord.write( dets[11] + '\n' )

                    op_planes.close()
                    op_ycoord.close()
        print( 'Files have been created and filled' )

    def plotting( self, params, names ): # TODO add the case if we want to consider only one of the stations
        #names = names['all']
        for station in names[ params[ 'part' ] ]:
            with open( params[ 'outdir' ] + station + '/value/DetectorNames.txt' ) as f:
                dets = f.read().splitlines()
                rmacro = PlotsCreate( 'Ycoord', params[ 'outdir' ] + station + '/', dets )
                for plane in dets:
                    rmacro.fillRootFile( plane )
            rmacro.drawAllStations( 'Ycoord', station, 'Y coordinate' )

    def showresults( self, params, names ):
        root.gROOT.SetBatch( False )
        root.gROOT.LoadMacro('./root_macros/ShowResults.c')
        root.ShowResults( params[ 'outdir' ], "Ycoord" )
