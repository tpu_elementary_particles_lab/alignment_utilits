#!/usr/bin/env python
"""
  @file		ShowResults.py
  @brief	
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>, Adam Szabelski <adam.szabelski@cern.ch>
  @version	$0.04 $
  @date		$Date: 03.01.2017 $
"""
import ROOT as root
import numpy

class ShowResults:

	def __init__( self, mode, outputdir, names ):
		self.mode = mode
		self.dir = outputdir
		self.names = names['all']
		#root.gROOT.SetBatch( True )

	def gui( self ):
		cmain = root.TCanvas( "cmain", "cmain", 50, 50, 1000, 600 )
		cmain.Divide( 8, 8 )
#		TVirtualPad *PadDet[ndets];
		#PadDet = root.TVirtualPad()
		PadDet = []
		print( self.names )
		for i, det in enumerate( self.names ):
			PadDetn = root.TPad()
			PadDetn = cmain.cd( i+1 )
			text = root.TPaveText( 0.01, 0.01, 0.98, 0.98 )
			text.AddText( det )
			text.Draw()
			'''
			Char_t cEx1[164], cEx2[164];
			sprintf(cEx1,"Draw%s%s%s", dets[i], path, mode) # name of command
			sprintf(cEx2,"DrawDet(\"%s\", \"%s\", \"%s\")", dets[i], path, mode) # the command
			PadDet.AddExec(cEx1, cEx2)
			'''
			print( 'Adding exec...' )
			PadDetn.AddExec('Draw'+det+self.dir+self.mode, 'TPython::Exec( "drawDet(\'' + det + '\')" )')
			PadDet.append( PadDetn )
		cmain.Update()
		raw_input('Type "Enter" to finish')

	def drawDet( det ):
		print('drawDet method call')
		select = root.gPad.GetSelected()
		if select is True: 
			return
		event = root.gPad.GetEvent()
		if event == 1:
			print("Vooala!")
			#root.gROOT->ProcessLine(Form(".x %s/%s/%s_all%s.C", path, det, det, mode))
			#root.gROOT->ProcessLine(Form(".x DrawAllDetectors.c(\"%s_%s\")", det,number))
