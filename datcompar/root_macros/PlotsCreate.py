#!/usr/bin/env python
"""
  @file		PlotsCreate.py
  @brief	
  @author	Alexandr Chumakov <alexandr.chumakov@cern.ch>, Adam Szabelski <adam.szabelski@cern.ch>
  @version	$0.03 $
  @date		$Date: 03.12.2016 $
"""
import ROOT as root
import numpy

class PlotsCreate:

	def __init__( self, mode, outputdir, names ):
		self.mode = mode
		#self.station = station
		self.dir = outputdir
		self.names = names
		self.skip_flag = 'absent value';
		root.gROOT.SetBatch( True )

	def fillRootFile( self, station ):
		# root.gROOT.SetBatch( True )
		zAverage, detDat, err1, err2 = [], [], [], []

		with open( self.dir + 'value/' + self.mode + '_' + station + '.txt' ) as f:
			#zAverage = filter(lambda x: x != skip_flag, f.read().splitlines() )
			for num, line in enumerate( f.read().splitlines() ):
				if line != self.skip_flag:
					zAverage.append( float(line) )
					detDat.append( float(num) )
					err1.append( 0.4 )
					err2.append( 0.0 )

		nn = len( zAverage )
		rfile = root.TFile( self.dir + 'root/' + 'graphs' + self.mode + '_' + station + '.root', 'recreate' )
		can = root.TCanvas( self.mode + '_' + station, self.mode + '_' + station, 400, 400, 800, 500 )
		graphs = root.TGraphErrors( nn, numpy.array( detDat ), numpy.array( zAverage ), numpy.array( err1 ), numpy.array( err2 ) )

		graphs.SetTitle(  self.mode + '_' + station )
		graphs.GetXaxis().SetTitle( 'detectors.dat' )
		graphs.GetXaxis().CenterTitle()
		graphs.GetYaxis().SetTitle( self.mode )
		graphs.GetYaxis().CenterTitle()
		graphs.SetLineWidth( 1 )
		graphs.SetMarkerSize( 15 )
		graphs.Draw( 'AP' )
		graphs.Write()

		#rfile.close() # commented due to very strange error: AttributeError: TFile object has no attribute 'close'

	def drawAllStations( self, mode, part, ytitle ):
		# debuging:
		#self.names = [ 'MP03' ]
		#self.names = [ 'MP01', 'MP02', 'DC01' ]
		#self.names = [ 'MP03', 'MP02' ]
		dats = open( self.dir + 'value/' + 'detectors.dat' ).read().splitlines()
		bins = len( dats )
		can = root.TCanvas( part, part, 200, 200, 950, 650 )

		pad_left = root.TPad("leftpad", "leftpad", 0.0, 0.0, 0.85, 1.0)
		pad_right = root.TPad("rightpad", "rightpad", 0.82, 0.0, 1.0, 0.9)
		pad_left.SetBottomMargin(0.2125749)
		pad_left.SetRightMargin(0.19125749)
		pad_right.Range(0,0,1,1)
		pad_left.Draw()
		pad_left.cd()

		gx, gy = root.Double(), root.Double()
		hs = root.THStack( 'hs', part )

		maxim = -100000.00
		minim = 100000.00

		leg = root.TLegend(0.0, 0.02, 0.9, 1.0)
		leg.SetNColumns(1)
		if len( self.names[0] ) == 4:
			leg.SetTextSize( 0.1 ) 
		else:
			leg.SetTextSize( 0.08 ) 

		for k, name in enumerate( self.names ):
			#print('Consider the name: ' + name)
			his = root.TH1D( 'h'+str(k+1), name+'histogram', bins, 0, float(bins) )
			his.SetLineColor(k%9+1)
			his.SetMarkerColor(k%9+1)
			his.SetMarkerSize(1.8)
			his.SetMarkerStyle(k%15+20)

			tfile = root.TFile( self.dir + 'root/' + 'graphs' + self.mode + '_' + name + '.root', 'readonly' )
			graph = tfile.Get( 'Graph' )

			c = 0
			for i in range( bins ):
				graph.GetPoint(c, gx, gy)
				#print('Points: x='+str(gx)+'	y='+str(gy) + '	binno=' + str(i+1) + '	counter=' + str(c))
				if int(gx) != i:
					#print('Bin skipping')
					continue
				if gy > maxim:
					maxim = float(gy)
					#print('Set the max as '+ str(maxim))
				if gy < minim:
					minim = float(gy)
					#print('Set the min as '+ str(minim))
				#print('Max = '+ str(maxim))
				his.SetBinContent(i+1, i, float(gy) )
				his.SetBinError(i+1, 0.000001)
				c += 1

			hs.Add( his )
			leg.AddEntry(his, ' ' + name.replace( '-', '   ' ), "pl")
			del tfile
			#del his
		offset = (maxim - minim)/10
		hs.SetMinimum( minim - offset )
		hs.SetMaximum( maxim + offset )
		hs.Draw("nostack")

		for i, line in enumerate( dats ):
			dats = line.split( ' ' )
			index = int( dats[0] ) + 1
			labels = dats[1].split( '.' )
			label = labels[1] + ' ' + labels[-2] + ' ' + str(i) 
			hs.GetXaxis().SetBinLabel( index, label )

		hs.GetXaxis().LabelsOption("v")
		#hs.GetXaxis().SetTitle("detectors.dat")
		hs.GetYaxis().SetTitle( self.mode )
		pad_left.Modified()
		pad_right.Draw()
		pad_right.cd()
		leg.Draw()

		can.Modified()
		can.Update()

		can.SaveAs( self.dir + 'macro/' + part + '_' + self.mode + 's.C' )
		can.SaveAs( self.dir + 'image/' + part + '_' + self.mode +'s.png' )
		can.SaveAs( self.dir + 'root/' + part + '_' + self.mode +'s.root' )
		# print( self.names )

