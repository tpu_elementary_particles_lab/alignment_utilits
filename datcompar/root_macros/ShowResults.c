
#define ndets 59


void ShowResults(const char *path="plots", const char *mode="Angle")
{
    Char_t dets[ndets][64] = { "FI01", "FI15", "FI02", "SI01", "SI02", "SI03", "FI03", "MP01", "MP02", "MP03", "FI04", "DC00", "DC01", "DC04", "DC05", "ST03", "FI05", "GM01", "GM02", "GM03", "PS01", "GM04", "GP02", "PA01", "GM05", "FI55", "FI06", "PA02", "GM06", "MA01", "MA02", "PA03", "PA04", "PA05", "GM07", "GM08", "GM09", "GP03", "FI07", "ST05", "DW01", "DW02", "DW03", "DW04", "DW05", "DW06", "PA06", "PA07", "GM10", "FI08", "PB01", "PB02", "MB01", "MB02", "PB03", "PB04", "PB05", "PB06", "GM11" };
    TCanvas *cmain = new TCanvas("cmain", "cmain", 50, 50, 1000, 600);
    cmain->Divide (8, 8);
        TVirtualPad *PadDet;
    for (int i=0; i<ndets; i++)
    {
        PadDet = (TVirtualPad*)cmain->cd(i+1);
        TPaveText *text= new TPaveText(0.01, 0.01, 0.98, 0.98);
        text->AddText(dets[i]);
        text->Draw();
    
        Char_t cEx1[164], cEx2[164];
        sprintf(cEx1,"Draw%s%s%s", dets[i], path, mode);
        sprintf(cEx2,"DrawDet(\"%s\", \"%s\", \"%s\")", dets[i], path, mode);
//        cout<< cEx2 << endl;  
        PadDet->AddExec(cEx1, cEx2);
    }

    
}

void DrawDet(const Char_t *det, const Char_t *path, const char *mode="Angle") 
{
    TObject *select = gPad->GetSelected();
    if(!select)   return;
    Int_t event = gPad -> GetEvent();
    if(event==1)
    {
        gROOT->ProcessLine(Form(".x %s/%s/macro/%s_%ss.C", path, det, det, mode));
//        gROOT->ProcessLine(Form(".x DrawAllDetectors.c(\"%s_%s\")", det,number));
    }
}
