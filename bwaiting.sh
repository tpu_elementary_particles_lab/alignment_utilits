#!/bin/bash

JOBNAME=$1

trun=`bjobs -J $JOBNAME | wc -l`
echo "trun=$trun: $JOBNAME "
while [ $trun -ne 0 ]
do
  echo "[`date '+%H:%M:%S'`] waiting for $JOBNAME "
  sleep 84
  trun=`bjobs -J $JOBNAME | wc -l`
done
