#!/usr/bin/env python
"""
  @file     run.py
  @brief    Executable file for run of the script
  @author   Alexandr Chumakov <alexandr.chumakov@cern.ch>
  @version  $0.01 $
  @date     $Date: 01.03.2018 $
"""
import sys, glob, getopt, os

from Parser import parse_geometry
from DBManager import DBManager
from Descriptor import Descriptor

# reading arguments
opts, args = getopt.getopt( sys.argv[ 1: ], 'm:i:o:r:', [ 'mode=', 'inputfile=', 'outputfile=', 'rulfiles=' ] )
for opt in opts:
    if opt[0] == '--mode' or opt[0] == '-m':
        params[ 'mode' ] = opt[1]
    elif opt[0] == '--inputfile' or opt[0] == '-i':
        params[ 'inputfile' ] = opt[1]
    elif opt[0] == '--outputfile' or opt[0] == '-o':
        params[ 'outfile' ] = opt[1]
    elif opt[0] == '--rulfiles' or opt[0] == '-r':
        params[ 'rulfiles' ] = opt[1]
        
   
rfile = open('/home/chu/hepsoft/compass/align_utils/tmp/detectors.2017.run278902.alon.start.dat', 'r' )
datlines = parse_geometry(rfile, ('mag', 'targ', 'rot', 'det', 'smtx', 'phot0', 'cath', 'mirr', 'mirr0', 'bbp', 'dead'))
rfile.close()
        


db = DBManager()
descr = Descriptor()
struct = descr.struct




data = {}
for k, v in datlines.items():
    if k in struct:
        mapping = descr.conformity[k]
        data[struct[k]] = []
        for lines in v:
            linedict = {'line_no': lines['lineno'], 'file_id': 1}
            if k == 'det' or k == 'dead':
                linedict['unique_key'] = lines['content'][mapping['tbname']]+lines['content'][mapping['det']]
            for col, index in mapping.items():
                if not index > len(lines['content'])-1:
                    #print('index '+ str(index))
                    #print('length '+ str(len(lines['content'])))
                    linedict.update({col: lines['content'][index]})
                        #print(line)
                        #exit
            data[struct[k]].append(linedict)
    else:
        print('current key! ' + str(len(v)))
        data['auxiliaries'] =  []
        for lines in v:
            data['auxiliaries'].append({'line_no': lines['lineno'], 'file_id': 1, 'content': lines['content']})
           
#print(datlines['@auxinfo'][0]['content'])

#print('length ' + str(len(data['detectors'])))
# {'detectors': [{lineno: , tbname: ..}, {}]}
db.datinput(data)

db.close()
