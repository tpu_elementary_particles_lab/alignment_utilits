#!/usr/bin/env python
"""
  @file    Descriptor.py
  @brief   Establishing a bijection between detectors.dat (indexes of column) and database structure
  @author  Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version $1.00$
  @date    $Date: 1.04.2018 $
"""

class Descriptor:

    def __init__(self):
        #self.data = data
        self.struct = {}
        self.struct['det']    = 'detectors'
        self.struct['dead']   = 'dead_zones'
        self.struct['cmtx']   = 'cmtx'
        self.struct['calo']   = 'calo'
        self.struct['rpd']    = 'rpd'
        self.struct['mag']    = 'mag'
        self.struct['rot']    = 'rot'
        self.struct['targ']   = 'target'
        self.struct['bbp']    = 'bbp'
        self.struct['phot0']  = 'rich_phot0'
        self.struct['cath']   = 'rich_cath'
        self.struct['mirr0']  = 'rich_mirr0'
        self.struct['mirr']   = 'rich_mirr'
        
        self.conformity = {
            'bbp':      self.bbp(),
            'calo':     self.calo(),
            'cmtx':     self.cmtx(),
            'dead':     self.dead_zones(),
            'det':      self.detectors(),
            'mag':      self.mag(),
            'cath':     self.rich_cath(),
            'mirr':     self.rich_mirr(),
            'mirr0':    self.rich_mirr0(),
            'phot0':    self.rich_phot0(),
            'rot':      self.rot(),
            'rpd':      self.rpd(),
            'targ':     self.target()
        }
        # must be separated out handler
        #self.struct['auxiliaries'] = self.auxiliaries()

    def detectors(self):
        lines = {}
        lines['det_id']      = 0
        lines['tbname']      = 1
        lines['det']         = 2
        lines['unit']        = 3
        lines['shape']       = 4
        lines['rad_len']     = 5
        lines['z_size']      = 6
        lines['x_size']      = 7
        lines['y_size']      = 8
        lines['z_center']    = 9
        lines['x_center']    = 10
        lines['y_center']    = 11
        lines['rot_matrix']  = 12
        lines['wire_dist']   = 13
        lines['angle']       = 14
        lines['nwires']      = 15
        lines['pitch']       = 16
        lines['effic']       = 17
        lines['bgnd']        = 18
        lines['time_gate']   = 19
        lines['vel']         = 20
        lines['t_zero']      = 21
        lines['res2hit']     = 22
        lines['space_res']   = 23
        lines['t_slice']     = 24
        return lines

    def dead_zones(self):
        lines = {}
        lines['det_id']      = 0
        lines['tbname']      = 1
        lines['det']         = 2
        lines['unit']        = 3
        lines['shape']       = 4
        lines['z_size']      = 5
        lines['x_size']      = 6
        lines['y_size']      = 7
        lines['z_center']    = 8
        lines['x_center']    = 9
        lines['y_center']    = 10
        lines['rot_matrix']  = 11
        return lines

    def cmtx(self):
        lines = {}
        lines['det_id']      = 0
        lines['det']         = 1
        lines['type']        = 2
        lines['nmod']        = 3
        lines['nrow']        = 4
        lines['ncol']        = 5
        lines['roffset']     = 6
        lines['coffset']     = 7
        lines['x_size']      = 8
        lines['y_size']      = 9
        lines['z_size']      = 10
        lines['x_fcell']     = 11
        lines['y_fcell']     = 12
        lines['z_fcell']     = 13
        lines['y_step']      = 14
        lines['z_step']      = 15
        lines['tgate']       = 16
        lines['tresh']       = 17
        lines['rad_len']     = 18
        lines['abs_len']     = 19
        lines['stocht']      = 20
        lines['constt']      = 21
        lines['reserv']      = 22
        lines['nholes']      = 23
        lines['idtype']      = 24
        lines['holepars']    = 25
        return lines

    def calo(self):
        lines = {}
        lines['tbname']      = 0
        lines['det']         = 1
        lines['z_coord']     = 2
        lines['x_coord']     = 3
        lines['y_coord']     = 4
        return lines

    def rpd(self):
        lines = {}
        lines['det_id']      = 0
        lines['det']         = 1
        return lines

    def mag(self):
        lines = {}
        lines['mag_no']      = 0
        lines['z_center']    = 1
        lines['x_center']    = 2
        lines['y_center']    = 3
        lines['rot_matrix']  = 4
        lines['field_scale'] = 5
        lines['flag1']       = 6
        lines['flag2']       = 7
        lines['current']     = 8
        return lines

    def rot(self):
        lines = {}
        lines['rot_id']      = 0 
        lines['cell_11']     = 1
        lines['cell_12']     = 2
        lines['cell_13']     = 3
        lines['cell_21']     = 4
        lines['cell_22']     = 5
        lines['cell_23']     = 6
        lines['cell_31']     = 7
        lines['cell_32']     = 8
        lines['cell_33']     = 9
        return lines

    def target(self):
        lines = {}
        lines['num']         = 0
        lines['name']        = 1
        lines['rot_matrix']  = 2
        lines['shape']       = 3
        lines['z_size']      = 4
        lines['x_size']      = 5
        lines['y_size']      = 6
        lines['z_center']    = 7
        lines['x_center']    = 8
        lines['y_center']    = 9
        return lines

    def bbp(self):
        lines = {}
        lines['tbname']    = 0
        lines['mean']      = 1
        lines['sigma']     = 2
        return lines
        
    def rich_phot0(self):
        lines = {}
        lines['name']      = 0
        lines['x_center']  = 1
        lines['y_center']  = 2
        lines['z_center']  = 3
        lines['rot_11']    = 4
        lines['rot_12']    = 5
        lines['rot_13']    = 6
        lines['rot_21']    = 7
        lines['rot_22']    = 8
        lines['rot_23']    = 9
        lines['rot_31']    = 10
        lines['rot_32']    = 11
        lines['rot_33']    = 12
        return lines

    def rich_cath(self):
        lines = {}
        lines['cath_id']   = 0
        lines['tbname']    = 1
        lines['name']      = 2
        lines['x_offset']  = 3
        lines['y_offset']  = 4
        lines['z_offset']  = 5
        lines['npadx']     = 6
        lines['npady']     = 7
        lines['padx']      = 8
        lines['pady']      = 9
        lines['qz_wind']   = 10
        lines['gap']       = 11
        lines['rot_11']    = 12
        lines['rot_12']    = 13
        lines['rot_13']    = 14
        lines['rot_21']    = 15
        lines['rot_22']    = 16
        lines['rot_23']    = 17
        lines['rot_31']    = 18
        lines['rot_32']    = 19
        lines['rot_33']    = 20
        return lines

    def rich_mirr0(self):
        lines = {}
        lines['name']      = 0
        lines['x_center']  = 1
        lines['y_center']  = 2
        lines['z_center']  = 3
        lines['radius']    = 4
        return lines

    def rich_mirr(self):
        lines = {}
        lines['name']      = 0 
        lines['theta']     = 1
        lines['phi']       = 2
        lines['radius']    = 3
        lines['d_theta']   = 4
        lines['d_phi']     = 5
        lines['delta']     = 6
        lines['qual']      = 7
        lines['notusd']    = 8
        return lines

    def auxiliaries(self):
        lines = {'content': ''}
        return lines
