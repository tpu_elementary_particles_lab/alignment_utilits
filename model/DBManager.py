#!/usr/bin/env python
"""
  @file    DBManager.py
  @brief   
  @author  Alexandr Chumakov <alexandr.achumakov@cern.ch>
  @version $1.00$
  @date    $Date: 5.04.2018 $
"""

import sqlite3, sys, shutil, random, string, os
from Descriptor import Descriptor

sys.path.insert(1, '../datparser')
from Parser import parse_geometry


class DBManager:

    def __init__(self, dbfile = False):
        self.userdb_flag = True
        if dbfile == False:
            self.userdb_flag = False
            # generate tempopary db, remove it after close of the connection
            dbfile = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
            thisdir = os.path.dirname(os.path.realpath(__file__))
            dbfile = thisdir + '/../tmp/'+dbfile+ '.sqlite3.db'
            shutil.copyfile(thisdir + '/../base/datfile_structure.sqlite3.db', dbfile)

        self.connection = sqlite3.connect(dbfile)
        self.dbfile = dbfile


    def preinput(self, datfile, fileid):
        data = {}
        
        descr = Descriptor()
        struct = descr.struct
        
        rfile = open(datfile, 'r')
        datlines = parse_geometry(rfile, tuple([x for x, v in struct.items()]))
        rfile.close()
        
        
        proj2nd = False
        for k, v in datlines.items():
            if k in struct:
                mapping = descr.conformity[k]
                data[struct[k]] = []
                for lines in v:
                    linedict = {'line_no': lines['lineno'], 'file_id': fileid}
                    if k == 'det' or k == 'dead':
                        tbname = lines['content'][mapping['tbname']]
                        linedict['unique_key'] = lines['content'][mapping['det_id']]+'-'+tbname+'-'+lines['content'][mapping['det']]
                        if (tbname[4:5] == 'M' or tbname[4:5] == 'P') and tbname[:2] != 'BM':
                            if proj2nd == False:
                                proj2nd = True
                                tbname_proj = tbname[:6] + '_U'
                            else:
                                proj2nd = False
                                tbname_proj = tbname[:6] + '_V'
                        else:
                            tbname_proj = tbname
                            proj2nd = False
                        linedict['tbname_proj'] = tbname_proj
                    for col, index in mapping.items():
                        if not index > len(lines['content'])-1:
                            linedict.update({col: lines['content'][index]})
                    data[struct[k]].append(linedict)
            else:
                data['auxiliaries'] =  []
                for lines in v:
                    data['auxiliaries'].append({'line_no': lines['lineno'], 'file_id': fileid, 'content': lines['content']})
        return data


    def datinput(self, data):
        for key, value in data.items():
            for line in value:
                fields = ''
                insert = ''
                for i, (k, item) in enumerate(line.items()):
                    fields = fields + k
                    insert = insert + "\"" + str(item).replace('"', "'") + "\""
                    if i < len(line)-1:
                        insert = insert + ', '
                        fields = fields + ', '
                self.connection.execute("INSERT INTO datfile_{t} ({f}) VALUES ({v})".format(t=key, f=fields, v=insert))
        self.connection.commit()
    
    def datoutput(self, file_id, newfile):
        data = []
      
        descr = Descriptor()
        struct = descr.struct
       
        cur = self.connection.cursor()
       
        for key, value in struct.items():
            fields = '`, `'.join([k for k, v in descr.conformity[key].items()])
            cur.execute("SELECT `line_no`, `"+fields+"` FROM `datfile_"+value+"` WHERE `file_id`='" +str(file_id)+ "'")
            resdb = cur.fetchall()

            colnames = cur.description
            for rows in resdb: 
                lineno = rows[0]
                #rows.pop(0)
                sortedline = range(len(rows)-1)
                for n, v in enumerate(rows):
                    col = colnames[n][0]
                    if col != 'line_no':
                        #minoffset = len(str(v))+3
                        minoffset = 3
                        if v < 0:
                            minoffset = minoffset - 1
                        if v == None:
                            v = '' # clean empty results from db (e.g. non drift-like detector description)
                        sortedline[descr.conformity[key][col]]= str(v).ljust(minoffset, ' ')

                line = '   '.join([x.encode('utf-8') for x in sortedline])
                line = ' '+key+'  '+line + " \n"
                data.append((lineno, line))

        cur.execute("SELECT `line_no`, `content` FROM `datfile_auxiliaries` WHERE `file_id`='" +str(file_id)+ "'")
        commsdb = cur.fetchall()
        for comment in commsdb:
            data.append((comment[0], comment[1].encode('utf-8')))


        sortedlist = range(len(data))

        for line in data:
            sortedlist[line[0]-1] = line[1]

        wf = open( newfile, 'w' )
        wf.write(''.join(sortedlist))
        wf.close()
        
        print('New file '+ os.path.realpath(newfile) + ' has been created')



    def close(self):
        self.connection.close()
        if self.userdb_flag == False:
            os.remove(self.dbfile) 
