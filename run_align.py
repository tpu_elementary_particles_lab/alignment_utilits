#!/usr/bin/env python
"""
  @file	preparation.py
  @brief  Make alignment procedure 
  @author  Alexandr Chumakov <alexandr.achumakov@cern.ch>, Evgenii Levchenko <evgenii.levchenko@cern.ch>
  @version $0.02 $
  @date	$Date: 24.10.2016 $
"""

import subprocess, os

def subprocess_cmd( command ):
    process = subprocess.Popen( command, stdout = subprocess.PIPE, shell=True )
    proc_stdout = process.communicate( )[0].strip( )
    print proc_stdout
	
execfile( 'sets.py' )

commands_list = [ 
	'cd ' + path_to_coral,
	'pwd',
	'source ' + path_to_coral + 'setup.sh',
	'cd src/alignment',
	#'./Linux/traf ' + run_dir + dirs_convention[ 'traf' ] + running_files[ 'traf_opt' ],
	'export HOST="lxplus"',
	'bsub -q 1nd -J T' + iteration + 'r' + run_number + ' "' + path_to_alignment + 'ManyRDs.csh -d ' + run_dir + dirs_convention[ 'traf_output' ] + ' -s 11001 -l ' + path_to_alignment + 'logs ' + run_dir + dirs_convention[ 'traf' ] + running_files[ 'traf_opt' ] + ' 28"',
	os.path.dirname(os.path.abspath(__file__)) + '/bwaiting.sh T' + iteration + 'r' + run_number,
	'bsub -q 1nd -J A' + iteration + 'r' + run_number + ' "'+ path_to_coral + '/src/alignment/Linux/align ' + run_dir + dirs_convention[ 'align' ] + running_files[ 'align_opt' ] + '"',
	os.path.dirname(os.path.abspath(__file__)) + '/bwaiting.sh A' + iteration + 'r' + run_number,
	'./Linux/updateFromAlign ' + run_dir + dirs_convention[ 'align_output' ] + 'align.W' + run_week + '.ai' + iteration + '.out' + ' ' 
							   + run_dir + dirs_convention[ 'geometry' ] + running_files[ 'geometry_opt' ] + ' ' 
							   + geometry_updated,
	'./Linux/checkTracks ' + run_dir + dirs_convention[ 'checktracks' ] + running_files[ 'checktracks_opt' ]
]

commands_str = ';\n\n '.join( commands_list )

print( commands_str )

# subprocess_cmd( commands_str )






